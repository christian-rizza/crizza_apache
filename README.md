## PHP-FPM Ubuntu based container

This is a Dockerfile/image to build a container with apache and php-fpm.
It contains multiple version of php and php-fpm

* PHP 7.4
* PHP 8.1


For the list of the php module installed, see [crizza/ubuntu](https://cloud.docker.com/u/crizza/repository/docker/crizza/ubuntu)

### Quick Start

To pull from docker hub:

`docker pull crizza/apache`

### Running

#####1. Without docker-compose

`docker run -d -p 80:80 crizza/apache` - PHP 7.4 by default

`docker run -d -p 80:80 -e "PHP_VERSION=7.4" crizza/apache` - PHP 7.4
`docker run -d -p 80:80 -e "PHP_VERSION=8.1" crizza/apache` - PHP 8.1

#####2. Using docker compose:

`docker-compose up -d ` - PHP version specified in docker-compose file

#####3. Check

Go to <http://localhost> to verify

### Switching php version

`docker-compose exec apache bash /opt/switcher.sh -v 7.4`
`docker-compose exec apache bash /opt/switcher.sh -v 8.1`
