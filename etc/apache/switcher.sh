#!/usr/bin/env bash

# Print usage
usage() {
  echo "$0 [OPTION]

PHP Version Switcher
--------------------
 Options:
  -v, --version     PHP Version
  -h, --help        Display this help and exit
"
}

switch () {

    if [[ $1 ]]
    then
        cd /etc/apache2/sites-available/

        # Check PHP Version to enable
        export PHP_VERSION=$1
        export HOST_IP=$(dig +short host.docker.internal)

        # Disable old php versions
        sudo a2dismod php*
        sudo a2disconf php*

		    # Stop previous services
        CURRENT_PHP_VERSION=$(php -r "echo join('.',(array_slice((explode('.',phpversion())),0,2)));")
        sudo service php$CURRENT_PHP_VERSION-fpm stop

        #Enable PHP-FPM and PHP Apache Modules
        sudo a2enmod actions alias proxy_fcgi
        sudo a2enconf php${PHP_VERSION}-fpm
        sudo a2enmod php${PHP_VERSION}

        #Configuring Apache
        sudo sed -i 's/max_execution_time = .*/max_execution_time = '600'/' /etc/php/${PHP_VERSION}/apache2/php.ini
        sudo sed -i 's/;phar.readonly = On/phar.readonly = Off/' /etc/php/${PHP_VERSION}/cli/php.ini
        sudo sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
        sudo sed -i "s/php*.*-fpm.sock/php\/php${PHP_VERSION}-fpm.sock/" /etc/apache2/sites-available/z-default.conf

        if [[ $HOST_IP ]]; then
            #Configuring XDebug
            echo -n "" | sudo tee /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "zend_extension=xdebug.so" | sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.idekey = PHPSTORM" | sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.default_enable = 0" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.remote_enable = 1" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.remote_autostart = 0" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.remote_connect_back = 0" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.profiler_enable = 0" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.remote_host = $HOST_IP" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.remote_port = 9000" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.remote_mode = req" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.max_nesting_level = 1024" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1

            #Configuration for Xdebug 3.x
            echo "xdebug.client_host = $HOST_IP" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.client_port = 9000" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.client_mode = req" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
            echo "xdebug.mode = debug" |sudo tee -a /etc/php/${PHP_VERSION}/mods-available/xdebug.ini > /dev/null 2>&1
        else
            sudo phpdismod xdebug #Make sure Xdebug is disabled
        fi

        sudo update-alternatives --set php /usr/bin/php${PHP_VERSION}
        sudo service apache2 start
        sudo service php${PHP_VERSION}-fpm start

        sudo a2ensite *
        sudo service apache2 reload

    else
        echo "PHP Version not found"
    fi
}

case $1 in
    -v | --version )        switch $2
                            ;;
    -h | --help )           usage
                            exit
                            ;;
    * )                     usage
                            exit 1
esac