#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

printLine () {
    echo "================================";
    echo "$1"
    echo "================================";
}

printLine "Apache - Setup Started"

###############################################################################
echo "Installing Apache and configure its modules..."
###############################################################################

sudo apt-get update > /dev/null 2>&1
sudo apt-get -y install apache2 > /dev/null 2>&1
sudo apt-get -y install libapache2-mod-php7.4 > /dev/null 2>&1
sudo apt-get -y install libapache2-mod-php8.1 > /dev/null 2>&1
sudo apt-get -y install libapache2-mod-fastcgi > /dev/null 2>&1

###############################################################################
echo "Removing default Apache configurations..."
###############################################################################

sudo a2dissite 000-default default-ssl
sudo rm /etc/apache2/sites-available/000-default.conf
sudo rm /etc/apache2/sites-available/default-ssl.conf
sudo a2ensite z-default
sudo a2enmod headers
sudo a2enmod rewrite

versions=$(ls /etc/php)
for v in $versions; do
    sudo sed -i 's,^memory_limit =.*$,memory_limit=1024M,' /etc/php/${v}/apache2/php.ini
    sudo sed -i 's,^memory_limit =.*$,memory_limit=1024M,' /etc/php/${v}/cli/php.ini
    sudo sed -i 's,^memory_limit =.*$,memory_limit=1024M,' /etc/php/${v}/fpm/php.ini

    sudo sed -i 's,^upload_max_filesize =.*$,upload_max_filesize=500M,' /etc/php/${v}/apache2/php.ini
    sudo sed -i 's,^upload_max_filesize =.*$,upload_max_filesize=500M,' /etc/php/${v}/cli/php.ini
    sudo sed -i 's,^upload_max_filesize =.*$,upload_max_filesize=500M,' /etc/php/${v}/fpm/php.ini

    sudo sed -i 's,^post_max_size =.*$,post_max_size=500M,' /etc/php/${v}/apache2/php.ini
    sudo sed -i 's,^post_max_size =.*$,post_max_size=500M,' /etc/php/${v}/cli/php.ini
    sudo sed -i 's,^post_max_size =.*$,post_max_size=500M,' /etc/php/${v}/fpm/php.ini

    sudo sed -i 's,^max_execution_time =.*$,max_execution_time=600,' /etc/php/${v}/apache2/php.ini
    sudo sed -i 's,^max_execution_time =.*$,max_execution_time=600,' /etc/php/${v}/cli/php.ini
    sudo sed -i 's,^max_execution_time =.*$,max_execution_time=600,' /etc/php/${v}/fpm/php.ini
done

echo "ServerName 127.0.0.1" | sudo tee -a /etc/apache2/apache2.conf

###############################################################################
echo "Removed unused files..."
###############################################################################

sudo cp /var/www/html/* /var/www/
sudo rm -rf /var/www/html
echo '<?php phpinfo(); ?>' | sudo tee /var/www/index.php > /dev/null 2>&1
sudo rm /var/www/index.html
sudo chown -R www-data:www-data /var/www

sudo rm -rf /var/lib/apt/lists/*

printLine "Apache - Setup Completed"
